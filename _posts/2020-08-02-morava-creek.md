---
title: Morava creek? Morava krík!
categories:
- General
feature_image: "/assets/morava-creek.jpg"
---

V pondělí před obědem volá Jirka že teče vršek Moravy který má vodu tak jednou do roka. Během oběda to zvažuju, a jdu do toho. Přidává sa ještě Adam takže budem tři. Vyrážám pro auto, pak na loděnici pro lodě, doráží Jirka, lodě jdou na střechu a jedem vyzvednout Adama. Původní plán byl vzít na pendl kolo, ale obvolávám spřízněné místní domorodce a ti nám udělají pendl, takže kolo zůstalo na loděnici a my po druhé hodině vyrážíme z Brna. Cesta po dálnici probíhá hladce, akorát za Zábřehem sme věřili navigaci a tak kličkujeme po uzkých rozbitých silničkách. Přesto však dorážíme před pátou na Dolní Moravu.


 * Nasedačka: první elektrárna
 * Vysedačka: v zatáčce u bunkru „KS-5 U potoka“
 * Vodní stav: vodočet Vlaské 7,6m³/s – 175cm
 * Délka: 5,7km
 * Čas: 1,5 hod (předtím hodinové rozmýšlení a nakoukávání)
 * Charakter: Dravé WWII-III, od Areálu snů vrbičkárna
 * Odkaz na Mapy: https://mapy.cz/s/mesogamape

_sepsal Majkl s přespěním Jirky a Adama_