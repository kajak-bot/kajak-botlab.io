---
title: Okibača potřebuje krev. Novou krev!
categories:
- General
feature_image: "/assets/kennedy-okibaca.jpg"
---

Vážení a milí kamarádi, pádleři – členové oddílu. Blíží se jarní nábor a byli bychom rádi, kdyby se povedlo dostat do oddílu nové lidi. Hlavně ale takové lidi, pro které by se Kajak stal životním stylem, tak jako pro mnohé z nás. Oddílu nejde o to být další “hodinou Jógy”, ale aktivní komunitou lidí co spolu jezdí na kajaku!

## “Tvoje Okibača Tě proto prosí, přiveď ji novou krev!”

Teď to hlavní, tradiční **Infoschůzka bude 26.3.** start tréninků pak od 30.3. – článek s podrobnostmi brzy, asi po Blatinách.. Zatím tedy hlavně uvažujte jak se zapojit!

**Tvoje Okibača Ti předem děkuje**
